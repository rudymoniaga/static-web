# Kubernetes deployment for Sample static HTML pages


## Name
Kubernetes deployment for Sample static HTML pages

## Assumption

To fulfill the criteria given, the following things are considered:

1. Run locally on minikube
2. No ingress/load balancer is installed, to access service, kubectl port forward command is used.
3. To make use the html code in the previous question, a docker image is created
4. To be able to connect to mysql server, mysql client is installed within the docker image
5. The following resources are created with community/third party provided helm charts:
    - MySql Server (bitnami/mysql)
    - Prometheus (prometheus-community/prometheus)
    - Grafana (bitnami/grafana)
    - Prometheus MySql exporter (prometheus-community/prometheus-mysql-exporter)
6. Pushing environment variable to html file is done by performing sed command by overriding docker entry point.
7. Monitoring only for MySQL server resources

## Result

The html website is able to connect to mysql server as below.

![screenshot-1](screenshot/screenshot-1.png)

The grafana dashboard 

![screenshot-2](screenshot/screenshot-2.png)

![screenshot-3](screenshot/screenshot-3.png)

![screenshot-4](screenshot/screenshot-4.png)

The html website

![screenshot-5](screenshot/screenshot-5.png)
