# Sample static HTML pages


## Name
Sample gitlab CI/CD pipeline for static HTML pages

## Description
The purpose of this project is for showcasing GitLab CI/CD with static HTML pages

## CI/CD Pipeline 

Normally CI/CD pipeline involve the following stages such as:

- Build
- Test 
- Release
- Deploy
- Validation

In our case, since this is only a static html pages, the following stages are set in .gitlab-ci.yml

- Build. No build needed to be done since html page doesn't need to be compile
- Test. I include SAST test to perform test on the javascript scripted in the html page
- Release. Each trigger will save the files into artifact
- Deploy. Deployment is handled by using GitLab Pages
- Validation. Validation is done manually and not part of the CI/CD pipeline
## Instructions:

1. Copy the html pages from https://www.geeksforgeeks.org/how-to-make-incremental-and-decremental-counter-using-html-css-and-javascript/ and combine into a HTML file called index.html.

2. Setup .gitlab-ci.yml

3. Commit code and push to gitlab. Pipeline will trigger automatically since it's already configured.

## Result

The html website can be accessible from https://rudymoniaga.gitlab.io/static-web/

![screenshot](screenshot/screenshot-1.png)